import SMRental.SMRental;
import SMRental.Seeds;
import cern.jet.random.engine.RandomSeedGenerator;

public class SME2 {

	public static void main(String[] args) {
		
		   int i, NUMRUNS = 10; 
	       double startTime=0.0, endTime=270.0;
	       Seeds[] sds = new Seeds[NUMRUNS];
	       SMRental SM;  // Simulation object

	       int[] vanCapacity=new int[] {12,18,30};       
	       
	       // Lets get a set of uncorrelated seeds
	       RandomSeedGenerator rsg = new RandomSeedGenerator();
	       for(i=0 ; i<NUMRUNS ; i++) sds[i] = new Seeds(rsg);
	       
	       // Loop for NUMRUN simulation runs for each case
	       // Case 1
	       
	      
	       for(i=0 ; i < NUMRUNS ; i++)
	       {
	    	  int numAgents=15;
	    	  int numVans=1;
	    	
	    	 
	          SM = new SMRental(startTime,endTime,numAgents,vanCapacity[0],numVans,sds[i], true);
	          SM.runSimulation();
	          double[] result=SM.getOutput();
	          System.out.println("Terminated: "+(i+1)+", customerSatisfaction: "+result[0]+", totalTravelDistance: "+result[1]);
	          
	    	  
	    	  
	       }

		
		
		
	}

}
/*
 int i, NUMRUNS = 10; 
	       double startTime=0.0, endTime=270.0;
	       Seeds[] sds = new Seeds[NUMRUNS];
	       SMRental SM;  // Simulation object

	       // Lets get a set of uncorrelated seeds
	       RandomSeedGenerator rsg = new RandomSeedGenerator();
	       for(i=0 ; i<NUMRUNS ; i++) sds[i] = new Seeds(rsg);
	       
	       // Loop for NUMRUN simulation runs for each case
	       // Case 1
	       
	      
	       for(i=0 ; i < NUMRUNS ; i++)
	       {
	          SM = new SMRental(startTime,endTime,1,1,12,sds[i], true);
	          SM.runSimulation();
	          System.out.println("Terminated "+(i+1));
	       }

*/