package SMRental;

import SMRental.Customer.numPassengers;

public class UDPs {

static SMRental model;
	

	public void UpdateOutputs(Customer icCustomer) 
	{
		
		
		model.output.totalCustomers+=1;
		double Totaltime = model.getClock()-icCustomer.startTime;
		
		if(icCustomer.direction=="ARRIVING")
		{
			if(Totaltime<Constants.CUSTOMER_SATISFIED_ARRIVING)
			{
				model.output.satisfiedCustomers+=1;
			}
		}
		if(icCustomer.direction=="DEPARTING")
		{
			if(Totaltime<Constants.CUSTOMER_SATISFIED_DEPARTING)
			{
				model.output.satisfiedCustomers+=1;
			}
		}
		
			
	}
	

	
	public int CanMove()
	{	
		int VanLineId=-1;
		
		for(int line=0;line<model.qVanLines.length;line++) {
			
			int[] condition=new int[] {-1,-1,-1};
			Vans van=model.qVanLines[line].qVans.get(0);
			
			//condition 1
			if(line==Constants.RENTAL) {
				if(van.finishedExiting==true) {
					condition[0]=1;	
				}
			}
			
			//condition 2 OR
			if(model.qCustomerLine[line].getN()==0) {
				condition[1]=1;	
			}
			
			//condition 2
			for(int customer=0;customer<model.qCustomerLine[line].getN();customer++) {
				int CustPass=model.qCustomerLine[line].CustLine.get(customer).nPassengers.getNum();
				
				if(CustPass<=van.vanCapacity - van.numPassengers) {
					condition[2]=1;
				}
			}
			
			if(condition[0]==1 && (condition[1]==1 || condition[2]!=1)) {
				VanLineId=line;
			}
		}
		return VanLineId;
	}
	
	public double GetDistance(int currentLocation, int destination)
	{
		double Distance=0;
		if(currentLocation==Constants.TERM1 && destination==Constants.TERM2)
		{
			Distance=0.3; 
		}
		if(currentLocation==Constants.TERM1 && destination==Constants.RENTAL)
		{
			Distance=1.5; 
		}
		if(currentLocation==Constants.TERM2 && destination==Constants.RENTAL)
		{
			Distance=2.0; 
		}
		if(currentLocation==Constants.RENTAL && destination==Constants.DROPOFF)
		{
			Distance=1.7; 
		}
		if(currentLocation==Constants.RENTAL && destination==Constants.TERM1)
		{
			Distance=1.5; 
		}
		return Distance;
	}
	
	public int GetNextVanDestination(Vans vanId,int currentLocation)
	{
		int nextLocation = 0;
		if(currentLocation==Constants.TERM1)
		{
			if(vanId.numPassengers==vanId.vanCapacity) {
				nextLocation=Constants.RENTAL;
			}
			else {
				nextLocation=Constants.TERM2;
			}
		}
		if(currentLocation==Constants.TERM2)
		{
				nextLocation=Constants.RENTAL;	
		}
		if(currentLocation==Constants.RENTAL)
		{
			if(vanId.numPassengers==0) {
				nextLocation=Constants.TERM1;
			}
			else {
				nextLocation=Constants.DROPOFF;
			}
		}
		if(currentLocation==Constants.DROPOFF)
		{
				nextLocation=Constants.TERM1;	
		}
		
		return nextLocation;
	}
	
	
	public int[] CanBoard()
	{
		int[] cust=new int[2];
		cust[0]=-1;
		cust[1]=-1;
		
		
		for(int i = 0; i<= model.qCustomerLine.length; i ++)
		{
			if(model.qCustomerLine[i].getN() > 0)
			{
				if(model.qVanLines[i].getN() > 0)
				{
					Vans m = model.qVanLines[i].qVans.get(0);
					for(int j=0;j<model.qCustomerLine.length; j ++) {
					Customer icCust=model.qCustomerLine[i].CustLine.get(j);
						
					
					if(icCust.nPassengers.getNum() < m.vanCapacity-m.numPassengers)
					{
						cust[0]=j;
						cust[1]=i;
					}
					
					}
					//(iC.Customer.numPassengers ≤ vanCapacity - Q.VanLine[lineId].list[0].numPassengers)
				}
			}
		}
		return (cust);
	}

	public int[] CanExit()
	{
		int[] van=new int[]{-1,-1};
		
		
		for(int vanlineid = Constants.RENTAL; vanlineid <= Constants.DROPOFF; vanlineid ++)
		{
			for(int vanid=0;vanid<=model.qVanLines[vanlineid].getN();vanid++) {
				if(model.qVanLines[vanlineid].qVans.get(vanid).numPassengers>0 &&
						(model.qVanLines[vanlineid].qVans.get(vanid).finishedExiting==false)){
					van[0]=vanlineid;
					van[1]=vanid;
				}
				
			}	
	    }
		return (van);
	}
	
}
