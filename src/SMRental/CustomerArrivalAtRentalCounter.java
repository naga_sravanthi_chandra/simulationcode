package SMRental;

import simulationModelling.ScheduledAction;

public class CustomerArrivalAtRentalCounter extends ScheduledAction {

	SMRental model; 
	CustomerArrivalAtRentalCounter(SMRental model) 
	{
		this.model = model;
	}
	@Override
	protected double timeSequence() {
		
		return(model.rvp.DuCRental());
	}

	@Override
	protected void actionEvent() {
		Customer icCustomer = new Customer();
	     icCustomer.startTime = model.getClock();
	     icCustomer.nPassengers = model.rvp.uNumPassengers();
	     icCustomer.direction = "DEPARTING";
	     model.qCustomerLine[Constants.CHECKIO].spInsertQue(icCustomer);
		
	}

}
