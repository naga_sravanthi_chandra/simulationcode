package SMRental;

import simulationModelling.ScheduledAction;

class Initialise extends ScheduledAction
{
	static SMRental model;
	
	double [] ts = { 0.0, -1.0 }; // -1.0 ends scheduling
	int tsix = 0;  // set index to first entry.
	
	public Initialise(SMRental smRental) {
		this.model=smRental;
		// TODO Auto-generated constructor stub
	}

	public double timeSequence() 
	{
		return ts[tsix++];  // only invoked at t=0
	}
	
	public void actionEvent() 
	{
		int id; 
		for(id = Constants.TERM1 ; id <= Constants.CHECKIO ; id++)
		{
			model.qCustomerLine[id].CustLine=null;  // Creates the object/entity
			
		}
		for(id = Constants.TERM1 ; id <= Constants.DROPOFF ; id++)
		{
			model.qVanLines[id].qVans=null; // Creates the object/entity
		
		}
		int Location=Constants.TERM1;
		 for(int i=0 ; i<=model.numVans ; i++)
		 {
			 model.rgVans[i].numPassengers = 0;
			 model.qVanLines[Location].spInsertQue(model.rgVans[i]);
			// model.rgVans[i].distanceTraveled =0;
			// int[] loc = {"Constants.TERM1", "TERM2", "RENTAL", "DROPOFF"};
			Location=(Location+1)%Constants.DROPOFF;
		 }
		 
		 model.output.totalTravelDistance=0;
		 model.output.totalCustomers=0;
		 model.output.satisfiedCustomers=0;
		 model.rgRentalCounter=null;
		 
		// model.rgRentalCounter.n = 0;
		 
	 
		}
}

