package SMRental;

import cern.jet.random.engine.RandomSeedGenerator;

public class Seeds 
{
	int PassengerN;   // Number of passengers
	int seed2;   // comment 2
	int seed3;   // comment 3
	int seed4;   // comment 4
	int exitT;

	public Seeds(RandomSeedGenerator rsg)
	{
		PassengerN=rsg.nextSeed();
		seed2=rsg.nextSeed();
		seed3=rsg.nextSeed();
		seed4=rsg.nextSeed();
		exitT=rsg.nextSeed();
	}
}
