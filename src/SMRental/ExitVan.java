package SMRental;

import simulationModelling.ConditionalActivity;

public class ExitVan extends  ConditionalActivity {
	
	SMRental model;
	Customer icCustomer;
	int vanLineid;
	int vanId;
	
	public ExitVan(SMRental model)
	{
		this.model=model;
	}

	public static boolean precondition(SMRental model) {
		int[] id = model.udp.CanExit();
		if(id[0] != -1 && id[1]!=-1)
			return true;
		else return false;
	}
	
	@Override
	public void startingEvent()
	{
		int[] id = model.udp.CanExit();
		vanLineid=id[0];
		vanId=id[1];
	}
	
	@Override
	protected double duration() {
		return model.rvp.uExitTime();
	}
	
	@Override
	protected void terminatingEvent() {
		
		
		
		icCustomer=model.rgVans[vanId].removeQue();
		
		if(vanLineid==Constants.DROPOFF)
		{
			model.udp.UpdateOutputs(icCustomer);
		}
		else {
			model.qCustomerLine[Constants.RENTAL].spInsertQue(icCustomer);
		}
		if(model.rgVans[vanId].getN()==0)
		{
			model.rgVans[vanId].finishedExiting = true;
		}		
	}
}
