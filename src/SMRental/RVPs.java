package SMRental;
import cern.jet.random.Empirical;
import cern.jet.random.EmpiricalWalker;
import cern.jet.random.Exponential;
import cern.jet.random.Uniform;
import cern.jet.random.engine.MersenneTwister;
import cern.jet.random.engine.MersenneTwister64;
import cern.jet.random.Normal;
import cern.jet.random.engine.RandomEngine;
class RVPs 
{
	SMRental model; // for accessing the clock
    // Data Models - i.e. random veriate generators for distributions
	// are created using Colt classes, define 
	// reference variables here and create the objects in the
	// constructor with seeds


	// Constructor
	protected RVPs(SMRental model, Seeds sd) 
	{ 
		this.model = model; 
		// Set up distribution functions
		numpassenger = new EmpiricalWalker(numPDF,
      		   Empirical.NO_INTERPOLATION,
      		   new MersenneTwister(sd.PassengerN));
		
		interArrDist = new Exponential(1.0/WMEAN1,  
				                       new MersenneTwister(sd.seed2));
		ExitTime = new Exponential(1 / Constants.EXIT_TIME, new MersenneTwister(sd.exitT));
	}
	
	/* Random Variate Procedure for Arrivals */
	private Exponential interArrDist;  // Exponential distribution for interarrival times
	private final double WMEAN1=10.0;
	protected double duInput()  // for getting next value of duInput
	{
	    double nxtInterArr;

        nxtInterArr = interArrDist.nextDouble();
	    // Note that interarrival time is added to current
	    // clock value to get the next arrival time.
	    return(nxtInterArr+model.getClock());
	   
	}
	
	//--------------------------------------------------------------------
		// Random Variate Procedure: uNumPassengers
		//--------------------------------------------------------------------
		// Define values for Number of Passengers and discrete PDF for generating Number of Passengers
		final static double Pone = 0.60;
		final static double Ptwo = 0.20;
		final static double Pthree = 0.15;
		final static double Pfour = 0.5;
		private final double [] numPDF = { Pone, Ptwo, Pthree, Pfour }; // for creating discrete PDF
		private EmpiricalWalker numpassenger; 
		Customer.numPassengers uNumPassengers()
		{
			Customer.numPassengers passengers;		
			switch(numpassenger.nextInt())
			{
			   case 0: passengers = Customer.numPassengers.ONE; break;
			   case 1: passengers = Customer.numPassengers.TWO; break;
			   case 2: passengers = Customer.numPassengers.THREE; break;
			   case 3: passengers = Customer.numPassengers.FOUR; break;
			   default: 
				   System.out.println("numPassenger returned invalid value");
				   passengers = Customer.numPassengers.ONE;		   	
			}
			return(passengers);
		}
		
		public double DuCTerm1()
		{
			double arrival_times_t1=0.0;
			double exponential_mean_t1;
			for(int t = 0; t <= 270 ; t++)
			{
				double poisson_mean_t1 = calculatePoissonMeanT1(t);
				exponential_mean_t1 = Math.exp(poisson_mean_t1);
				arrival_times_t1 = t + exponential_mean_t1;	
			}
			return arrival_times_t1;			
		}
		
		public double DuCTerm2() 
		{
			double arrival_times_t2=0.0;
			double exponential_mean_t2;
			for(int t = 0; t <= 270 ; t++)
			{
				double poisson_mean_t2 = calculatePoissonMeanT2(t);
				exponential_mean_t2 = Math.exp(poisson_mean_t2);
				arrival_times_t2 = t + exponential_mean_t2;	
			}
			return arrival_times_t2;	
		}

		public double uRentalCIOTime(String direction) {
			if(direction=="ARRIVING") {
				double a=0.768;
				
				double mean1=2.185;
				double stdev1=0.325;
				double g1=Normal.staticNextDouble(mean1, stdev1);
				
				double mean2=2.185;
				double stdev2=0.325;
				
				double g2=Normal.staticNextDouble(mean2, stdev2);
				
				return( (a*g1) + ((1-a)*g2) );
				
			}
			else {
				double a=0.866;
				
				double mean1=1.450;
				double stdev1=0.261;
				double g1=Normal.staticNextDouble(mean1, stdev1);
				
				double mean2=3.986;
				double stdev2=0.448;
				
				double g2=Normal.staticNextDouble(mean2, stdev2);
				
				return( (a*g1) + ((1-a)*g2) );
			}
		}

		public double DuCRental()
		{
			double arrival_times_rc=0.0;
			double exponential_mean_rc;
			for(int t = 0; t <= 270 ; t++)
			{
				double poisson_mean_rc = calculatePoissonMeanRC(t);
				exponential_mean_rc = Math.exp(poisson_mean_rc);
				arrival_times_rc = t + exponential_mean_rc;	
			}
			return arrival_times_rc;			
			}	
			
		public double calculatePoissonMeanRC(int t)
		{
			double poisson_mean_rc=0.0;
			switch(t)
			{
			case 0: if(t >= 0 && t < 15)
				poisson_mean_rc = 5;
				break;
			case 1: if(t >= 15 && t < 30)
				poisson_mean_rc = 6.666666667;
				break;
			case 2: if(t >= 30 && t < 45)
				poisson_mean_rc = 3.333333333;
				break;
			case 3: if(t >= 45 && t < 60)
				poisson_mean_rc = 2.142857143;
				break;
			case 4: if(t >= 60 && t < 75)
				poisson_mean_rc = 2.608695652;
				break;
			case 5: if(t >= 75 && t < 90)
				poisson_mean_rc = 2.857142857;
				break;
			case 6: if(t >= 90 && t < 105)
				poisson_mean_rc = 3.75;
				break;
			case 7: if(t >= 105 && t < 120)
				poisson_mean_rc = 5.454545455;
				break;
			case 8: if(t >= 120 && t < 135)
				poisson_mean_rc = 3.529411765;
				break;
			case 9: if(t >= 135 && t < 150)
				poisson_mean_rc = 2.0;
				break;
			case 10: if(t >= 150 && t < 165)
				poisson_mean_rc = 1.666666667;
				break;
			case 11: if(t >= 165 && t < 180)
				poisson_mean_rc = 2.5;
				break;
			case 12: if(t >= 180 && t < 195)
				poisson_mean_rc = 1.875;
				break;
			case 13: if(t >= 195 && t < 210)
				poisson_mean_rc = 3.75;
				break;
			case 14: if(t >= 210 && t < 225)
				poisson_mean_rc = 4.615384615;
				break;
			case 15: if(t >= 225 && t < 240)
				poisson_mean_rc = 4.615384615;
				break;
			case 16: if(t >= 240 && t < 255)
				poisson_mean_rc = 12;
				break;
			case 17: if(t >= 255 && t < 270)
				poisson_mean_rc = 15;
				break;
		}
			return poisson_mean_rc;
		}
		
		public double calculatePoissonMeanT1(int t)
		{
			double poisson_mean_t1=0.0;
			switch(t)
			{
			case 0: if(t >= 0 && t < 15)
				poisson_mean_t1 = 15;
				break;
			case 1: if(t >= 15 && t < 30)
				poisson_mean_t1 = 17.5;
				break;
			case 2: if(t >= 30 && t < 45)
				poisson_mean_t1 = 5;
				break;
			case 3: if(t >= 45 && t < 60)
				poisson_mean_t1 = 4;
				break;
			case 4: if(t >= 60 && t < 75)
				poisson_mean_t1 = 3.333333333;
				break;
			case 5: if(t >= 75 && t < 90)
				poisson_mean_t1 = 4.285714286;
				break;
			case 6: if(t >= 90 && t < 105)
				poisson_mean_t1 = 4.615384615;
				break;
			case 7: if(t >= 105 && t < 120)
				poisson_mean_t1 = 6;
				break;
			case 8: if(t >= 120 && t < 135)
				poisson_mean_t1 = 15;
				break;
			case 9: if(t >= 135 && t < 150)
				poisson_mean_t1 = 2.0;
				break;
			case 10: if(t >= 150 && t < 165)
				poisson_mean_t1 = 6;
				break;
			case 11: if(t >= 165 && t < 180)
				poisson_mean_t1 = 4.285714286;
				break;
			case 12: if(t >= 180 && t < 195)
				poisson_mean_t1 = 3.75;
				break;
			case 13: if(t >= 195 && t < 210)
				poisson_mean_t1 = 4;
				break;
			case 14: if(t >= 210 && t < 225)
				poisson_mean_t1 = 8.571428571;
				break;
			case 15: if(t >= 225 && t < 240)
				poisson_mean_t1 = 20;
				break;
			case 16: if(t >= 240 && t < 255)
				poisson_mean_t1 = 15;
				break;
			case 17: if(t >= 255 && t < 270)
				poisson_mean_t1 = 30;
				break;
		}
			return poisson_mean_t1;
		}
		
		public double calculatePoissonMeanT2(int t)
		{
			double poisson_mean_t2=0.0;
			switch(t)
			{
			case 0: if(t >= 0 && t < 15)
				poisson_mean_t2 = 3;
				break;
			case 1: if(t >= 15 && t < 30)
				poisson_mean_t2 = 6;
				break;
			case 2: if(t >= 30 && t < 45)
				poisson_mean_t2 = 9;
				break;
			case 3: if(t >= 45 && t < 60)
				poisson_mean_t2 = 15;
				break;
			case 4: if(t >= 60 && t < 75)
				poisson_mean_t2 = 17;
				break;
			case 5: if(t >= 75 && t < 90)
				poisson_mean_t2 = 19;
				break;
			case 6: if(t >= 90 && t < 105)
				poisson_mean_t2 = 14;
				break;
			case 7: if(t >= 105 && t < 120)
				poisson_mean_t2 = 6;
				break;
			case 8: if(t >= 120 && t < 135)
				poisson_mean_t2 = 3;
				break;
			case 9: if(t >= 135 && t < 150)
				poisson_mean_t2 = 2.0;
				break;
			case 10: if(t >= 150 && t < 165)
				poisson_mean_t2 = 21;
				break;
			case 11: if(t >= 165 && t < 180)
				poisson_mean_t2 = 14;
				break;
			case 12: if(t >= 180 && t < 195)
				poisson_mean_t2 = 19;
				break;
			case 13: if(t >= 195 && t < 210)
				poisson_mean_t2 = 12;
				break;
			case 14: if(t >= 210 && t < 225)
				poisson_mean_t2 = 5;
				break;
			case 15: if(t >= 225 && t < 240)
				poisson_mean_t2 = 2;
				break;
			case 16: if(t >= 240 && t < 255)
				poisson_mean_t2 = 3;
				break;
			case 17: if(t >= 255 && t < 270)
				poisson_mean_t2 = 3;
				break;
		}
			return poisson_mean_t2;
		}
		static Exponential ExitTime;

		public double uExitTime() {
			
			return ExitTime.nextDouble();
		}
}

		
	
	

