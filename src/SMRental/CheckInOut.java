package SMRental;
import simulationModelling.ConditionalActivity;

public class CheckInOut extends ConditionalActivity {
	
	SMRental model;
	Customer icCustomer;
	
	public CheckInOut(SMRental model) { this.model=model;}
	
	public static boolean precondition(SMRental smModel) {
		return (smModel.rgRentalCounter.getN() < smModel.rgRentalCounter.numAgents) && 
				(smModel.qCustomerLine[Constants.RENTAL].getN() > 0);
	}
	@Override
	protected double duration() {
		// TODO Auto-generated method stub
		return (model.rvp.uRentalCIOTime(icCustomer.direction));
	}

	@Override
	public void startingEvent() {
		
		icCustomer= model.qCustomerLine[Constants.CHECKIO].Remove(0);		
		model.rgRentalCounter.insertGrp(icCustomer);
		
	}

	@Override
	protected void terminatingEvent() {
		model.rgRentalCounter.removeGrp(icCustomer);
		if(icCustomer.direction=="DEPARTING")
		{
			model.udp.UpdateOutputs(icCustomer);
		// No need to implement SP.Leave - Java has a garbage collector
			
		}
		else {
			model.qCustomerLine[Constants.RENTAL].spInsertQue(icCustomer);
			}
	}
}