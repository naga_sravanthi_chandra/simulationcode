package SMRental;


import simulationModelling.AOSimulationModel;
import simulationModelling.Behaviour;

public class SMRental extends AOSimulationModel {
	
	// Parameters
	int numVans ;

	// Entities
	protected Vans [] rgVans = new Vans[numVans];
	protected CustomerLine [] qCustomerLine = new CustomerLine[4];
	protected VanLines [] qVanLines = new VanLines[4];
	protected RentalCounter rgRentalCounter = new RentalCounter();
	//protected Customer [] cust = new Customer[];

	// Random variate procedures
	RVPs rvp;
	
	// Determinate Variate Procedures
	DVPs dvp=new DVPs();
	
	// User Defined Procedures
	UDPs udp = new UDPs();
	
	// Outputs
    Output output = new Output();
    
    public double[] getOutput() {
    	double[] result=new double[2];
    	result[0]=output.customerSatisfaction;
    	result[1]=output.totalTravelDistance;
    	return result; 
    }
    
    
    // Constructor
	public SMRental(double t0time, double tftime,int numAgents, int vanCapacity,int numVans,Seeds sd, boolean log) {
		
		
		// Adding references to model object to classes
		
		// For turning on logging
		traceFlag = log;
		
		// Create RVP object with given seed
		rvp = new RVPs(this, sd);
		rgRentalCounter.numAgents=numAgents;
		
		for(int i=0;i<numVans;i++) {
			rgVans[i].vanCapacity=vanCapacity;
		}
		this.numVans=numVans;
		
		initAOSimulModel(t0time, tftime+60);  // set stop condition to ensure SBL is not empty.
		

		// Schedule the first arrivals and employee scheduling
		Initialise init = new Initialise(this);
		scheduleAction(init); // Should always be first one scheduled.
		
		CustomerArrivalAtTerminal1 arrivalT1 = new CustomerArrivalAtTerminal1(this);
		scheduleAction(arrivalT1); // customer arrival at  terminal 1
		CustomerArrivalAtTerminal2 arrivalT2 = new CustomerArrivalAtTerminal2(this);
		scheduleAction(arrivalT2); // customer arrival at  terminal 2
		CustomerArrivalAtRentalCounter arrivalRC = new CustomerArrivalAtRentalCounter(this);
		scheduleAction(arrivalRC); // customer arrival at Rental Counter

	}
	

		@Override
		protected void testPreconditions(Behaviour behObj) {
			
			reschedule(behObj);
			while (BoardVan.precondition(this) == true) {
				BoardVan boardC = new BoardVan(this);
				boardC.startingEvent();
				scheduleActivity(boardC);
			}
			while (MoveToNextVanLine.precondition(this) == true) {
				MoveToNextVanLine move = new MoveToNextVanLine(this);
				move.startingEvent();
				scheduleActivity(move);
			}
			while (ExitVan.precondition(this) == true) {
				ExitVan exitvan = new ExitVan(this);
				exitvan.startingEvent();
				scheduleActivity(exitvan);
			}
			while (CheckInOut.precondition(this) == true) {
				CheckInOut checkio = new CheckInOut(this);
				checkio.startingEvent();
				scheduleActivity(checkio);
			}
			
		}
		public  void print1() {
			 System.out.println("Clock: "+getClock());
		}
		// Flag for controlling tracing
		boolean traceFlag = false;
		public void eventOccured() {
			
			if(traceFlag)
			{
				 System.out.println("Clock: "+getClock());
				 showSBL();			
			}
			 
		}
		
		
		
}

