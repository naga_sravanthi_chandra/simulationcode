package SMRental;

import cern.jet.random.Exponential;
import cern.jet.random.engine.MersenneTwister;
import simulationModelling.ScheduledAction;

public class CustomerArrivalAtTerminal1 extends ScheduledAction {
	
	SMRental model; // For accessing the complete model
	CustomerArrivalAtTerminal1(SMRental model) { this.model = model; }
	
	
	public double timeSequence() 
	{
		return(model.rvp.DuCTerm1());
	}

	public void actionEvent() 
	{
		// CustomerArrivalAtTerminal1 Action Sequence SCS
		 Customer icCustomer = new Customer();
	     icCustomer.startTime = model.getClock();
	     icCustomer.nPassengers=model.rvp.uNumPassengers();
	     icCustomer.direction="ARRIVING";
	     model.qCustomerLine[Constants.TERM1].spInsertQue(icCustomer);
	}

}
