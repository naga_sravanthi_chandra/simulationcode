package SMRental;

import simulationModelling.ScheduledAction;

public class CustomerArrivalAtTerminal2 extends ScheduledAction{
	
	SMRental model; // For accessing the complete model
	CustomerArrivalAtTerminal2(SMRental model) { this.model = model; }
	
	
	public double timeSequence() 
	{
		return(model.rvp.DuCTerm2());
	}

	public void actionEvent() 
	{
		// CustomerArrivalAtTerminal2 Action Sequence SCS
		 Customer icCustomer = new Customer();
	     icCustomer.startTime = model.getClock();
	     icCustomer.nPassengers=model.rvp.uNumPassengers();
	     icCustomer.direction="ARRIVING";
	     model.qCustomerLine[Constants.TERM2].spInsertQue(icCustomer);
	}

}
