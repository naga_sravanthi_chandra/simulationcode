package SMRental;

public class DVPs {

	SMRental model;
	
	public double uTravelTime(int origin, int destination){
		double time = 0;
		if(origin==Constants.TERM1 && destination==Constants.TERM2)
		{
			time=0.9; 
		}
		if(origin==Constants.TERM1 && destination==Constants.RENTAL)
		{
			time=4.5; 
		}
		if(origin==Constants.TERM2 && destination==Constants.RENTAL)
		{
			time=6.0; 
		}
		if(origin==Constants.RENTAL && destination==Constants.DROPOFF)
		{
			time=5.1; 
		}
		if(origin==Constants.RENTAL && destination==Constants.TERM1)
		{
			time=4.5; 
		}
		return time;
		
	}
	
	
}
