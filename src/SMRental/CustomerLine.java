package SMRental;

import java.util.ArrayList;



public class CustomerLine {
	
	/* List of customer waiting in line */
	protected ArrayList<Customer> CustLine = new ArrayList<Customer>();
	protected int getN() 
	{ 
		return CustLine.size(); 
	}  
	
	// Attribute n
	protected void spInsertQue(Customer cust)
	{ 
		CustLine.add(cust); 
	}	
	protected Customer Remove(int custpos)
	{
		Customer customer=new Customer();
		if(CustLine.size()!=0) customer=CustLine.remove(custpos);
		return(customer);
	}
	
}
