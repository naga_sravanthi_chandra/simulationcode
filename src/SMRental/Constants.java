package SMRental;
  
public class Constants {

	/* Constants */
	// Define constants as static
	// Example: protected final static double realConstant = 8.0;
	public static final double PATH_DISTANCE[] = {1.5, 1.7, 0.5, 0.3, 2.0};
	public static final double VAN_SPEED = 20;
	public static final double CUSTOMER_SATISFIED_ARRIVING = 20;
	public static final double CUSTOMER_SATISFIED_DEPARTING = 18;
	public static final double COST_PER_MILE[] = {0.43, 0.73, 0.92};
	public static final int TERM1=0;
	public static final int TERM2=1;
	public static final int RENTAL=2;
	public static final int CHECKIO=3;
	public static final int DROPOFF = 3;
	public static final int EXIT_TIME=6;
	public static final int BOARD_TIME = 12;
	
}
