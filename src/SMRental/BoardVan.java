package SMRental;

import SMRental.Customer.numPassengers;
import simulationModelling.ConditionalActivity;

public class BoardVan extends ConditionalActivity
{
	SMRental model;
	Customer icCustomer;
	Vans van;
	UDPs udp;
	int LineId;
	int custPos;
	Vans idvan;
	public BoardVan(SMRental model)
	{
		this.model=model;		
	}
	
	public static boolean precondition(SMRental model)
	{
		int[] id = model.udp.CanBoard();
		
		if(id[0] != -1 && id[1]!=-1)
			return true;
		else return false;
	}
	
	@Override
	public double duration() 
	{
		return Constants.BOARD_TIME;
	}
	
	@Override
	public void startingEvent()
	{
		int[] id = model.udp.CanBoard();
		this.LineId=id[1];
		this.custPos=id[0];
		idvan = model.qVanLines[LineId].qVans.get(0);		
	}
	
	@Override
	protected void terminatingEvent()
	{
		int pass;
		
		icCustomer= model.qCustomerLine[LineId].Remove(custPos);		
		idvan.insertQue(icCustomer);
		
		
		
		idvan.numPassengers+=icCustomer.nPassengers.getNum();
		
		
//		iC.Customer ← SP.RemoveQue(Q.CustomerLines[lineId],custpos)
//		SP.InsertGrp(RG.Vans[id], iC.Customer)
//		RG.Vans[id].numPassengers +← iC.Customer.numPassengers

	}
}
