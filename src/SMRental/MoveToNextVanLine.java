package SMRental;
import SMRental.SMRental;
import simulationModelling.ConditionalActivity;


public class MoveToNextVanLine extends ConditionalActivity
{

	SMRental model; 
	Customer icTanker; 
	Vans vanId;
	VanLines vanLineId;
	int currentLocation;
	int destination;

		
	public MoveToNextVanLine(SMRental model)
	{
		this.model = model;
	}
	
	public static boolean precondition(SMRental model)
	{
		return  (model.udp.CanMove() !=-1);
	}
	
	@Override
	public void startingEvent() 
	{
		currentLocation = model.udp.CanMove();
		vanId= model.qVanLines[currentLocation].spRemoveQue();
		destination = model.udp.GetNextVanDestination(vanId, currentLocation);
	}
	
	@Override
	public double duration() 
	{
		return (model.dvp.uTravelTime(currentLocation, destination));
	}
	
	@Override
	public void terminatingEvent() 
	{
		model.qVanLines[destination].spInsertQue(vanId);
		model.output.totalTravelDistance+=model.udp.GetDistance(currentLocation, destination);
		vanId.finishedExiting=false;
		//model.rgVans[vanId].finishedExiting=false;
		
	}

}
